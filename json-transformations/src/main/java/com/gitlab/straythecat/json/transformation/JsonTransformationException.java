package com.gitlab.straythecat.json.transformation;

public class JsonTransformationException extends RuntimeException {
	public JsonTransformationException() {
	}

	public JsonTransformationException(String message) {
		super(message);
	}

	public JsonTransformationException(String message, Throwable cause) {
		super(message, cause);
	}

	public JsonTransformationException(Throwable cause) {
		super(cause);
	}
}
