package com.gitlab.straythecat.json.transformation;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.Objects;
import java.util.function.Predicate;

@FunctionalInterface
public interface JsonTransformation {
	void apply(JsonObject origin, JsonObjectBuilder transformed);

	default JsonTransformation andThen(JsonTransformation after) {
		Objects.requireNonNull(after);

		return (o, b) -> {
			apply(o, b);
			after.apply(o, b);
		};
	}

	default JsonTransformation guarded(Predicate<JsonObject> predicate) {
		Objects.requireNonNull(predicate);

		return (o, b) -> {
			if (predicate.test(o)) {
				apply(o, b);
			}
		};
	}
}
