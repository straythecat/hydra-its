package com.gitlab.straythecat.json.transformation;

import javax.json.*;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collector;

//TODO: consider handling and rethrowing wrapped exceptions in place to produce meaningful error messages
public class JsonTransformations {
	private final JsonBuilderFactory json;

	public JsonTransformations(JsonBuilderFactory json) {
		this.json = json;
	}

	public JsonObject apply(JsonTransformation transformation, JsonObject object) {
		try {
			JsonObjectBuilder builder = json.createObjectBuilder();
			transformation.apply(object, builder);
			return builder.build();
		} catch (RuntimeException exception) {
			throw new JsonTransformationException(exception);
		}
	}

	public JsonArray apply(JsonTransformation transformation, JsonArray array) {
		try {
			return array.stream().map(v -> apply(transformation, v.asJsonObject())).collect(toJsonArray());
		} catch (JsonTransformationException exception) {
			throw exception;
		} catch (RuntimeException exception) {
			throw new JsonTransformationException(exception);
		}
	}

	public JsonTransformation require(String key) {
		Objects.requireNonNull(key);

		return (o, b) -> b.add(key, o.get(key));
	}

	public JsonTransformation retain(String key) {
		Objects.requireNonNull(key);

		return (o, b) -> {
			JsonValue v = o.get(key);
			if (v != null) {
				b.add(key, v);
			}
		};
	}

	public JsonTransformation requireNewName(String oldKey, String newKey) {
		Objects.requireNonNull(oldKey);
		Objects.requireNonNull(newKey);

		return (o, b) -> b.add(newKey, o.get(oldKey));
	}

	public JsonTransformation rename(String oldKey, String newKey) {
		Objects.requireNonNull(oldKey);
		Objects.requireNonNull(newKey);

		return (o, b) -> {
			JsonValue v = o.get(oldKey);
			if (v != null) {
				b.add(newKey, v);
			}
		};
	}

	public JsonTransformation transformValue(String key, UnaryOperator<JsonValue> operator) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(operator);

		return (o, b) -> b.add(key, operator.apply(o.get(key)));
	}

	public JsonTransformation transformArrayValue(String key, JsonTransformation transformation) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(transformation);

		return (o, b) -> b.add(key, apply(transformation, o.get(key).asJsonArray()));
	}

	public JsonTransformation transformValue(String oldKey, String newKey, UnaryOperator<JsonValue> operator) {
		Objects.requireNonNull(oldKey);
		Objects.requireNonNull(newKey);
		Objects.requireNonNull(operator);

		return (o, b) -> b.add(newKey, operator.apply(o.get(oldKey)));
	}

	public JsonTransformation transformArrayValue(String oldKey, String newKey, JsonTransformation transformation) {
		Objects.requireNonNull(oldKey);
		Objects.requireNonNull(newKey);
		Objects.requireNonNull(transformation);

		return (o, b) -> b.add(newKey, apply(transformation, o.get(oldKey).asJsonArray()));
	}

	public JsonTransformation create(String key, JsonValue value) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(value);

		return (o, b) -> b.add(key, value);
	}

	public JsonTransformation create(String key, Supplier<JsonValue> supplier) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(supplier);

		return (o, b) -> b.add(key, supplier.get());
	}

	public JsonTransformation compound(JsonTransformation ...transformations) {
		Objects.requireNonNull(transformations);

		return (o, b) -> {
			for (JsonTransformation transformation : transformations) {
				transformation.apply(o, b);
			}
		};
	}

	public JsonTransformation compose(JsonTransformation inner, JsonTransformation outer){
		Objects.requireNonNull(inner);
		Objects.requireNonNull(outer);

		return (o, b) -> outer.apply(apply(inner, o), b);
	}

	public JsonTransformation skip() {
		return (o, b) -> {};
	}

	private Collector<JsonValue, JsonArrayBuilder, JsonArray> toJsonArray() {
		return Collector.of(json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::addAll, JsonArrayBuilder::build);
	}
}
