package com.gitlab.straythecat.hydra.redmine.connectivity.support.collections;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Set;

/**
 * It is useful in case when a consumer of a set only iterates over its elements.
 *
 * @param <T> the type of elements maintained by this set
 *
 * @see Set
 */
public class ArraySet<T> extends AbstractSet<T> {
	private final T[] array;

	public ArraySet(T[] array) {
		this.array = array;
	}

	@Override
	public Iterator<T> iterator() {
		return new ArrayIterator<>(array);
	}

	@Override
	public int size() {
		return array.length;
	}
}
