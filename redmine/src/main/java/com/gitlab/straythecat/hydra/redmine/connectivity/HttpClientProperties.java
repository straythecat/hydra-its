package com.gitlab.straythecat.hydra.redmine.connectivity;

public interface HttpClientProperties {
	int getConnectionPoolSize();
	int getMaxPooledPerRoute();
}
