package com.gitlab.straythecat.hydra.redmine.connectivity.support.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<T> implements Iterator<T> {
	private final T[] array;
	private int cursor = 0;

	public ArrayIterator(T[] array) {
		this.array = array;
	}

	@Override
	public boolean hasNext() {
		return cursor < array.length;
	}

	@Override
	public T next() {
		try {
			return array[cursor++];
		} catch (ArrayIndexOutOfBoundsException exception) {
			throw new NoSuchElementException();
		}
	}
}
