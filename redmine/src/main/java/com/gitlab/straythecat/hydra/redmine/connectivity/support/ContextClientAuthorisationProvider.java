package com.gitlab.straythecat.hydra.redmine.connectivity.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.gitlab.straythecat.hydra.redmine.connectivity.authorisation.ClientAuthorisationProvider;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

@Service
public class ContextClientAuthorisationProvider implements ClientAuthorisationProvider {
	private final Logger logger = LoggerFactory.getLogger(ContextClientAuthorisationProvider.class);

	private final HttpServletRequest requestProxy;

	public ContextClientAuthorisationProvider(HttpServletRequest requestProxy) {
		this.requestProxy = requestProxy;
	}

	@Override
	public void provideAuthorisation(ClientRequestContext requestContext) {
		String authorisation = null;

		try {
			authorisation = requestProxy.getHeader(HttpHeaders.AUTHORIZATION);
		} catch (IllegalStateException exception) {
			logger.error("Thread-bound request has been not found.", exception);
		}

		if (authorisation != null) {
			MultivaluedMap<String, Object> headers = requestContext.getHeaders();
			if (!headers.containsKey(HttpHeaders.AUTHORIZATION)) {
				headers.putSingle(HttpHeaders.AUTHORIZATION, authorisation);
			}
		}
	}
}
