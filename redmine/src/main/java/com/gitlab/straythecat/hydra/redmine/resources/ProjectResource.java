package com.gitlab.straythecat.hydra.redmine.resources;

import com.gitlab.straythecat.hydra.redmine.support.MediaTypes;
import com.gitlab.straythecat.json.transformation.JsonTransformation;
import com.gitlab.straythecat.json.transformation.JsonTransformations;
import com.gitlab.straythecat.hydra.redmine.services.ProjectService;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@CrossOrigin(exposedHeaders = HttpHeaders.LINK)
@RequestMapping(path = "/project", produces = MediaTypes.JSON_LD_VALUE)
public class ProjectResource {
	private final ProjectService service;
	private final JsonTransformations transformations;

	private final JsonTransformation projectsTransformation;
	private final JsonTransformation projectTransformation;

	public ProjectResource(ProjectService service, JsonTransformations transformations) {
		this.service = service;
		this.transformations = transformations;

		projectTransformation = transformations.compound(
				transformations.create("@context", () -> Json.createValue(fromMethodCall(on(ContextResource.class).getProjectContext()).toUriString())),
				transformations.transformValue("id", "@id", (v) ->
						Json.createValue(fromMethodCall(on(ProjectResource.class).getProject(((JsonNumber) v).longValueExact())).toUriString())),
				transformations.create("@type", Json.createValue("Project")),
				transformations.retain("name"),
				transformations.retain("description"),
				transformations.transformValue("id", "issues", (v) ->
						Json.createValue(fromMethodCall(on(IssueResource.class).getIssues(((JsonNumber) v).longValueExact())).toUriString()))
		);

		projectsTransformation = transformations.compound(
				transformations.create("@context", () -> Json.createValue(fromMethodCall(on(ContextResource.class).getProjectCollectionContext()).toUriString())),
				transformations.create("@id", () -> Json.createValue(fromMethodCall(on(ProjectResource.class).getProjects()).toUriString())),
				transformations.create("@type", Json.createValue("ProjectCollection")),
				transformations.transformArrayValue("projects", projectTransformation)
		);
	}

	@GetMapping
	public JsonObject getProjects() {
		JsonObject projects = service.getProjects();
		projects = transformations.apply(projectsTransformation, projects);
		return projects;
	}

	@GetMapping("/{id}")
	public JsonObject getProject(@PathVariable("id") long id) {
		JsonObject project = service.getProject(id);
		project = transformations.apply(projectTransformation, project.getJsonObject("project"));
		return project;
	}
}
