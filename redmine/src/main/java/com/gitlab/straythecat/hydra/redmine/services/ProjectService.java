package com.gitlab.straythecat.hydra.redmine.services;

import org.springframework.stereotype.Service;

import javax.json.JsonObject;
import javax.ws.rs.client.WebTarget;

@Service
public class ProjectService {
	private final WebTarget projects;
	private final WebTarget project;

	public ProjectService(WebTarget service) {
		projects = service.path("/projects.json");
		project = service.path("/projects/{id}.json");
	}

	public JsonObject getProjects() {
		return projects.request().get(JsonObject.class);
	}

	public JsonObject getProject(long id) {
		return project.resolveTemplate("id", id).request().get(JsonObject.class);
	}
}
