package com.gitlab.straythecat.hydra.redmine.services;

import org.springframework.stereotype.Service;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.WebTarget;

@Service
public class IssueService {
	private final WebTarget issues;
	private final WebTarget issue;

	public IssueService(WebTarget service) {
		issues = service.path("/issues.json");
		issue = service.path("/issues/{id}.json");
	}

	public JsonObject getIssues(Long projectId) {
		WebTarget issues = this.issues;
		if (projectId != null) {
			issues = issues.queryParam("project_id", projectId);
		}
		return issues.request().get(JsonObject.class);
	}

	public JsonObject getIssue(long id) {
		return issue.resolveTemplate("id", id).request().get(JsonObject.class);
	}
}
