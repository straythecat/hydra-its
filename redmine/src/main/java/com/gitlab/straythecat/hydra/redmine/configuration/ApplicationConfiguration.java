package com.gitlab.straythecat.hydra.redmine.configuration;

import com.gitlab.straythecat.hydra.redmine.support.DocumentationLinkInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@PropertySource("classpath:app.properties")
public class ApplicationConfiguration {

	@Configuration
	@EnableWebSecurity
	public static class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable();
		}
	}

	@Configuration
	public static class WebConfiguration extends WebMvcConfigurerAdapter {
		private final HandlerInterceptor documentationLinkInterceptor;

		public WebConfiguration(DocumentationLinkInterceptor documentationLinkInterceptor) {
			this.documentationLinkInterceptor = documentationLinkInterceptor;
		}

		@Override
		public void addInterceptors(InterceptorRegistry registry) {
			registry.addInterceptor(documentationLinkInterceptor);
		}
	}
}
