package com.gitlab.straythecat.hydra.redmine.resources;

import com.gitlab.straythecat.hydra.redmine.support.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@CrossOrigin(exposedHeaders = HttpHeaders.LINK)
@RequestMapping(path = "/context", produces = MediaTypes.JSON_LD_VALUE)
public class ContextResource {
	private final JsonBuilderFactory json;

	public ContextResource(JsonBuilderFactory json) {
		this.json = json;
	}

	@GetMapping("/EntryPoint")
	public JsonObject getEntryPointContext() {
		return json.createObjectBuilder().
				add("@context", json.createObjectBuilder().
						add("@vocab", resolveDocumentationUri()).
						add("hydra", "http://www.w3.org/ns/hydra/core#").
						add("projects", json.createObjectBuilder().
								add("@id", "EntryPoint/projects").
								add("@type", "@id")).
						add("issues", json.createObjectBuilder().
								add("@id", "EntryPoint/issues").
								add("@type", "@id"))).
				build();
	}

	@GetMapping("/ProjectCollection")
	public JsonObject getProjectCollectionContext() {
		return json.createObjectBuilder().
				add("@context", json.createObjectBuilder().
						add("@vocab", resolveDocumentationUri()).
						add("hydra", "http://www.w3.org/ns/hydra/core#").
						add("projects", "http://www.w3.org/ns/hydra/core#member")).
				build();
	}

	@GetMapping("/Project")
	public JsonObject getProjectContext() {
		return json.createObjectBuilder().
				add("@context", json.createObjectBuilder().
						add("@vocab", resolveDocumentationUri()).
						add("hydra", "http://www.w3.org/ns/hydra/core#").
						add("name", "http://schema.org/name").
						add("description", "http://schema.org/description").
						add("issues", json.createObjectBuilder().
								add("@id", "Project/issues").
								add("@type", "@id"))).
				build();
	}

	@GetMapping("/IssueCollection")
	public JsonObject getIssueCollectionContext() {
		return json.createObjectBuilder().
				add("@context", json.createObjectBuilder().
						add("@vocab", resolveDocumentationUri()).
						add("hydra", "http://www.w3.org/ns/hydra/core#").
						add("issues", "http://www.w3.org/ns/hydra/core#member")).
				build();
	}

	@GetMapping("/Issue")
	public JsonObject getIssueContext() {
		return json.createObjectBuilder().
				add("@context", json.createObjectBuilder().
						add("@vocab", resolveDocumentationUri()).
						add("hydra", "http://www.w3.org/ns/hydra/core#").
						add("subject", "http://schema.org/name").
						add("description", "http://schema.org/description")).
				build();
	}

	private String resolveDocumentationUri() {
		return fromMethodCall(on(DocumentationResource.class).getDocumentation()).toUriString();
	}
}
