package com.gitlab.straythecat.hydra.redmine.connectivity.support;

import com.gitlab.straythecat.hydra.redmine.connectivity.ConnectivityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.net.URI;

@Component
@ConfigurationProperties("hydra-its.connectivity")
public class SpringBootConnectivityProperties implements ConnectivityProperties {
	@NotNull
	private URI serviceUrl;

	@Override
	public URI getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(URI serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
}
