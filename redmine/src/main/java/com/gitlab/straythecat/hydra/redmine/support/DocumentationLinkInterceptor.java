package com.gitlab.straythecat.hydra.redmine.support;

import com.gitlab.straythecat.hydra.redmine.resources.DocumentationResource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@Component
public class DocumentationLinkInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String documentationUri = fromMethodCall(on(DocumentationResource.class).getDocumentation()).toUriString();
		String link = String.format(Locale.ROOT, "<%s>; rel=\"http://www.w3.org/ns/hydra/core#apiDocumentation\"", documentationUri);
		response.addHeader("Link", link);
		return true;
	}
}
