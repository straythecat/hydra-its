package com.gitlab.straythecat.hydra.redmine.connectivity;

import com.gitlab.straythecat.hydra.redmine.logging.LoggingClientResponseFilter;
import com.gitlab.straythecat.hydra.redmine.serialisation.ObjectMapperProvider;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@Configuration
public class ConnectivityConfiguration {
	private final ClientBuilder clientBuilder;

	public ConnectivityConfiguration(ObjectMapperProvider mapperProvider, LoggingClientResponseFilter loggingFilter, HttpClientProperties httpClientProperties) {
		clientBuilder = ClientBuilder.newBuilder().
				register(mapperProvider).
				register(loggingFilter);

		((ResteasyClientBuilder) clientBuilder).
				connectionPoolSize(httpClientProperties.getConnectionPoolSize()).
				maxPooledPerRoute(httpClientProperties.getMaxPooledPerRoute());
	}

	@Bean(destroyMethod = "close")
	public Client client(){
		return clientBuilder.build();
	}

	@Bean
	public WebTarget service(Client client, ConnectivityProperties properties) {
		return client.target(properties.getServiceUrl());
	}
}
