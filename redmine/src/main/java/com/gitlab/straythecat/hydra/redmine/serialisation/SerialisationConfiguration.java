package com.gitlab.straythecat.hydra.redmine.serialisation;

import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr353.JSR353Module;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import com.gitlab.straythecat.json.transformation.JsonTransformations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import java.util.Collections;

@Configuration
public class SerialisationConfiguration {

	@Bean
	public JsonBuilderFactory jsonBuilderFactory() {
		return Json.createBuilderFactory(Collections.emptyMap());
	}

	@Bean
	public JSR353Module jsr353Module(){
		return new JSR353Module();
	}

	@Bean
	public AfterburnerModule afterburnerModule(){
		return new AfterburnerModule();
	}

	@Bean
	public Jdk8Module jdk8Module(){
		return new Jdk8Module();
	}

	@Bean
	public JavaTimeModule javaTimeModule(){
		return new JavaTimeModule();
	}

	@Bean
	public JsonTransformations jsonTransformations(JsonBuilderFactory json) {
		return new JsonTransformations(json);
	}
}
