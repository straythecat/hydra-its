package com.gitlab.straythecat.hydra.redmine.connectivity;

import java.net.URI;

public interface ConnectivityProperties {
	URI getServiceUrl();
}
