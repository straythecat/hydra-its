package com.gitlab.straythecat.hydra.redmine.resources;

import com.gitlab.straythecat.hydra.redmine.services.IssueService;
import com.gitlab.straythecat.hydra.redmine.support.MediaTypes;
import com.gitlab.straythecat.json.transformation.JsonTransformation;
import com.gitlab.straythecat.json.transformation.JsonTransformations;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.json.Json;
import javax.json.JsonNumber;
import javax.json.JsonObject;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@CrossOrigin(exposedHeaders = HttpHeaders.LINK)
@RequestMapping(path = "/issue", produces = MediaTypes.JSON_LD_VALUE)
public class IssueResource {
	private final IssueService service;
	private final JsonTransformations transformations;

	private final JsonTransformation issuesTransformation;
	private final JsonTransformation issueTransformation;

	public IssueResource(IssueService service, JsonTransformations transformations) {
		this.service = service;
		this.transformations = transformations;

		issueTransformation = transformations.compound(
				transformations.create("@context", () -> Json.createValue(fromMethodCall(on(ContextResource.class).getIssueContext()).toUriString())),
				transformations.transformValue("id", "@id", (v) ->
						Json.createValue(fromMethodCall(on(IssueResource.class).getIssue(((JsonNumber) v).longValueExact())).toUriString())),
				transformations.create("@type", Json.createValue("Issue")),
				transformations.retain("subject"),
				transformations.retain("description")
		);
		issuesTransformation = transformations.compound(
				transformations.create("@context", () -> Json.createValue(fromMethodCall(on(ContextResource.class).getIssueCollectionContext()).toUriString())),
				transformations.create("@type", Json.createValue("IssueCollection")),
				transformations.transformArrayValue("issues", issueTransformation)
		);
	}

	@GetMapping
	public JsonObject getIssues(@RequestParam(name = "project_id", required = false) Long projectId) {
		JsonObject issues = service.getIssues(projectId);
		issues = transformations.apply(issuesTransformation.andThen(
				transformations.create("@id", Json.createValue(fromMethodCall(on(IssueResource.class).getIssues(projectId)).toUriString()))),
				issues);
		return issues;
	}

	@GetMapping("/{id}")
	public JsonObject getIssue(@PathVariable("id") long id) {
		JsonObject issue = service.getIssue(id);
		issue = transformations.apply(issueTransformation, issue.getJsonObject("issue"));
		return issue;
	}
}
