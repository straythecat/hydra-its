package com.gitlab.straythecat.hydra.redmine.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.straythecat.hydra.redmine.support.MediaTypes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.json.JsonArray;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@CrossOrigin(exposedHeaders = HttpHeaders.LINK)
@RequestMapping(path = "/documentation", produces = MediaTypes.JSON_LD_VALUE)
public class DocumentationResource {
	private final JsonBuilderFactory json;

	private final JsonObject context;
	private final JsonArray classes;

	public DocumentationResource(ApplicationContext context, ObjectMapper objectMapper, JsonBuilderFactory json) {
		this.json = json;

		this.context = readJson(objectMapper, context.getResource("classpath:/documentation/context.json"));

		this.classes = json.createArrayBuilder().
				add(readJson(objectMapper, context.getResource("classpath:/documentation/collection.json"))).
				add(readJson(objectMapper, context.getResource("classpath:/documentation/resource.json"))).
				add(readJson(objectMapper, context.getResource("classpath:/documentation/entry-point.json"))).
				add(readJson(objectMapper, context.getResource("classpath:/documentation/project.json"))).
				add(readJson(objectMapper, context.getResource("classpath:/documentation/project-collection.json"))).
				add(readJson(objectMapper, context.getResource("classpath:/documentation/issue.json"))).
				add(readJson(objectMapper, context.getResource("classpath:/documentation/issue-collection.json"))).
				build();
	}

	@GetMapping
	public Object getDocumentation() {
		return json.createObjectBuilder().
				add("@context", json.createObjectBuilder(context).add("vocab", resolveDocumentationUri())).
				add("@id", resolveDocumentationUri()).
				add("@type", "ApiDocumentation").
				add("supportedClass", classes).
				build();
	}

	private JsonObject readJson(ObjectMapper objectMapper, Resource resource) {
		try {
			return objectMapper.readValue(resource.getFile(), JsonObject.class);
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	private String resolveDocumentationUri() {
		return fromMethodCall(on(DocumentationResource.class).getDocumentation()).toUriString();
	}
}
