package com.gitlab.straythecat.hydra.redmine.serialisation;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Provider
@Component
public class ObjectMapperProvider implements ContextResolver<ObjectMapper> {
	private final ObjectMapper mapper;

	public ObjectMapperProvider(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public ObjectMapper getContext(Class<?> aClass) {
		return mapper;
	}

}
