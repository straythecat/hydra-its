package com.gitlab.straythecat.hydra.redmine.resources;

import com.gitlab.straythecat.hydra.redmine.support.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@CrossOrigin(exposedHeaders = HttpHeaders.LINK)
@RequestMapping(produces = MediaTypes.JSON_LD_VALUE)
public class EntryPointResource {
	private final JsonBuilderFactory json;

	public EntryPointResource(JsonBuilderFactory json) {
		this.json = json;
	}

	@GetMapping
	public JsonObject getEntryPoint() {
		return json.createObjectBuilder().
				add("@context", fromMethodCall(on(ContextResource.class).getEntryPointContext()).toUriString()).
				add("@id", fromMethodCall(on(EntryPointResource.class).getEntryPoint()).toUriString()).
				add("@type", "EntryPoint").
				add("projects", fromMethodCall(on(ProjectResource.class).getProjects()).toUriString()).
				add("issues", fromMethodCall(on(IssueResource.class).getIssues(null)).toUriString()).
				build();
	}
}
