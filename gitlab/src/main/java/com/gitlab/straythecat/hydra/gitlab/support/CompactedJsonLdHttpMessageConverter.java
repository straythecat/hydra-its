package com.gitlab.straythecat.hydra.gitlab.support;

import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Component
public class CompactedJsonLdHttpMessageConverter implements HttpMessageConverter<JsonObject> {
	private final List<MediaType> supportedMediaTypes = Collections.singletonList(MediaTypes.JSON_LD);
	private final JsonLdOptions options = new JsonLdOptions();

	private final JsonBuilderFactory json;

	public CompactedJsonLdHttpMessageConverter(JsonBuilderFactory json) {
		this.json = json;
	}

	@Override
	public boolean canRead(Class aClass, MediaType mediaType) {
		return aClass.equals(JsonObject.class) &&
				mediaType.getType().equals(MediaTypes.JSON_LD.getType()) &&
				mediaType.getSubtype().equals(MediaTypes.JSON_LD.getSubtype());
	}

	@Override
	public boolean canWrite(Class aClass, MediaType mediaType) {
		return false;
	}

	@Override
	public List<MediaType> getSupportedMediaTypes() {
		return supportedMediaTypes;
	}

	@Override
	public JsonObject read(Class aClass, HttpInputMessage httpInputMessage) throws IOException, HttpMessageNotReadableException {
		try {
			return json.createObjectBuilder(JsonLdProcessor.compact(JsonUtils.fromInputStream(httpInputMessage.getBody()), null, options)).build();
		} catch (Exception exception) {
			throw new HttpMessageNotReadableException("JSON-LD error", exception);
		}
	}

	@Override
	public void write(JsonObject o, MediaType mediaType, HttpOutputMessage httpOutputMessage) throws IOException, HttpMessageNotWritableException {
		throw new UnsupportedOperationException();
	}
}
