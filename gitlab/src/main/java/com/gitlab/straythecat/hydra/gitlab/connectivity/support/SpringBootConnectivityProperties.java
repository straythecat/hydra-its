package com.gitlab.straythecat.hydra.gitlab.connectivity.support;

import com.gitlab.straythecat.hydra.gitlab.connectivity.ConnectivityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.net.URI;

@Component
@ConfigurationProperties("hydra-its.connectivity")
public class SpringBootConnectivityProperties implements ConnectivityProperties {
	@NotNull
	private URI serviceUrl;

	private String authorisationHeader;
	private String authorisationValue;

	@Override
	public URI getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(URI serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	@Override
	public String getAuthorisationHeader() {
		return authorisationHeader;
	}

	public void setAuthorisationHeader(String authorisationHeader) {
		this.authorisationHeader = authorisationHeader;
	}

	@Override
	public String getAuthorisationValue() {
		return authorisationValue;
	}

	public void setAuthorisationValue(String authorisationValue) {
		this.authorisationValue = authorisationValue;
	}
}
