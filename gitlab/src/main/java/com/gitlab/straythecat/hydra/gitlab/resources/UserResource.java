package com.gitlab.straythecat.hydra.gitlab.resources;

import com.gitlab.straythecat.hydra.gitlab.services.UserService;
import com.gitlab.straythecat.hydra.gitlab.support.MediaTypes;
import com.gitlab.straythecat.json.transformation.JsonTransformation;
import com.gitlab.straythecat.json.transformation.JsonTransformations;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@CrossOrigin(exposedHeaders = HttpHeaders.LINK)
@RequestMapping(produces = MediaTypes.JSON_LD_VALUE)
public class UserResource {
	private final UserService service;
	private final JsonTransformations transformations;
	private final JsonBuilderFactory json;

	private final JsonTransformation userTransformation;

	public UserResource(UserService service, JsonTransformations transformations, JsonBuilderFactory json) {
		this.service = service;
		this.transformations = transformations;
		this.json = json;

		userTransformation = transformations.compound(
				transformations.create("@context", () ->
						Json.createValue(fromMethodCall(on(ContextResource.class).getUserContext()).toUriString())),
				transformations.create("@type", Json.createValue("User")),
				transformations.retain("name"),
				transformations.retain("username")
		);
	}

	private JsonTransformation requireId(long projectId) {
		return (o, b) -> b.add("@id", Json.createValue(
				fromMethodCall(on(UserResource.class).
						getUser(projectId, o.getJsonNumber("id").longValueExact())).toUriString()));
	}

	@GetMapping(path = "/projects/{projectId}/members")
	public JsonObject getUsers(@PathVariable("projectId") long projectId) {
		JsonArray users = service.getUsers(projectId);
		return json.createObjectBuilder().
				add("@context", fromMethodCall(on(ContextResource.class).getUserCollectionContext()).toUriString()).
				add("@type", "UserCollection").
				add("@id", fromMethodCall(on(UserResource.class).getUsers(projectId)).toUriString()).
				add("users", transformations.apply(userTransformation.andThen(requireId(projectId)), users)).
				build();
	}

	@GetMapping("/projects/{projectId}/members/{userId}")
	public JsonObject getUser(@PathVariable("projectId") long projectId, @PathVariable("userId") long userId) {
		JsonObject user = service.getUser(projectId, userId);
		user = transformations.apply(userTransformation.andThen(requireId(projectId)), user);
		return user;
	}
}
