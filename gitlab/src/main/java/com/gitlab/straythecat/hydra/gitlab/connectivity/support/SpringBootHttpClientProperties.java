package com.gitlab.straythecat.hydra.gitlab.connectivity.support;

import com.gitlab.straythecat.hydra.gitlab.connectivity.HttpClientProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Min;

@Component
@ConfigurationProperties("hydra-its.connectivity.httpClient")
public class SpringBootHttpClientProperties implements HttpClientProperties {
	@Min(0)
	private int connectionPoolSize;
	@Min(0)
	private int maxPooledPerRoute;

	@Override
	public int getConnectionPoolSize() {
		return connectionPoolSize;
	}

	@Override
	public int getMaxPooledPerRoute() {
		return maxPooledPerRoute;
	}

	public void setConnectionPoolSize(int connectionPoolSize) {
		this.connectionPoolSize = connectionPoolSize;
	}

	public void setMaxPooledPerRoute(int maxPooledPerRoute) {
		this.maxPooledPerRoute = maxPooledPerRoute;
	}
}
