package com.gitlab.straythecat.hydra.gitlab.resources;

import com.gitlab.straythecat.hydra.gitlab.services.ProjectService;
import com.gitlab.straythecat.hydra.gitlab.support.MediaTypes;
import com.gitlab.straythecat.json.transformation.JsonTransformation;
import com.gitlab.straythecat.json.transformation.JsonTransformations;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.json.*;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@CrossOrigin(exposedHeaders = HttpHeaders.LINK)
@RequestMapping(path = "/projects", produces = MediaTypes.JSON_LD_VALUE)
public class ProjectResource {
	private final ProjectService service;
	private final JsonTransformations transformations;
	private final JsonBuilderFactory json;

	private final JsonTransformation projectTransformation;

	public ProjectResource(ProjectService service, JsonTransformations transformations, JsonBuilderFactory json) {
		this.service = service;
		this.transformations = transformations;
		this.json = json;

		projectTransformation = transformations.compound(
				transformations.create("@context", () -> Json.createValue(fromMethodCall(on(ContextResource.class).getProjectContext()).toUriString())),
				transformations.transformValue("id", "@id", (v) ->
						Json.createValue(fromMethodCall(on(ProjectResource.class).getProject(((JsonNumber) v).longValueExact())).toUriString())),
				transformations.create("@type", Json.createValue("Project")),
				transformations.retain("name"),
				transformations.retain("description"),
				transformations.transformValue("id", "issues", (v) ->
						Json.createValue(fromMethodCall(on(IssueResource.class).getIssues(((JsonNumber) v).longValueExact())).toUriString())),
				transformations.transformValue("id", "members", (v) ->
						Json.createValue(fromMethodCall(on(UserResource.class).getUsers(((JsonNumber) v).longValueExact())).toUriString()))
		);
	}

	@GetMapping
	public JsonObject getProjects() {
		JsonArray projects = service.getProjects();
		return json.createObjectBuilder().
				add("@context", fromMethodCall(on(ContextResource.class).getProjectCollectionContext()).toUriString()).
				add("@id", fromMethodCall(on(ProjectResource.class).getProjects()).toUriString()).
				add("@type", "ProjectCollection").
				add("projects", transformations.apply(projectTransformation, projects)).
				build();
	}

	@GetMapping("/{id}")
	public JsonObject getProject(@PathVariable("id") long id) {
		JsonObject project = service.getProject(id);
		project = transformations.apply(projectTransformation, project);
		return project;
	}
}
