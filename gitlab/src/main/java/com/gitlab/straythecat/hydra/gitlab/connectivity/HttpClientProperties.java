package com.gitlab.straythecat.hydra.gitlab.connectivity;

public interface HttpClientProperties {
	int getConnectionPoolSize();
	int getMaxPooledPerRoute();
}
