package com.gitlab.straythecat.hydra.gitlab.connectivity.support.collections;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

/**
 * It is useful in case when a consumer of a map only iterates over its key-value pairs.
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 *
 * @see Map
 */
public class ArrayMap<K, V> extends AbstractMap<K, V> {
	private final ArraySet<Entry<K, V>> set;

	public ArrayMap(Entry<K, V>[] array) {
		this.set = new ArraySet<>(array);
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return set;
	}

	@SafeVarargs
	public static <K, V> ArrayMap<K, V> create(Entry<K, V> ...entries) {
		return new ArrayMap<>(entries);
	}

	public static <K, V> Entry<K, V> entry(K key, V value) {
		return new SimpleImmutableEntry<>(key, value);
	}
}
