package com.gitlab.straythecat.hydra.gitlab.connectivity.support;

import com.gitlab.straythecat.hydra.gitlab.connectivity.authorisation.ClientAuthorisationProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.core.MultivaluedMap;

public class PredefinedClientAuthorisationProvider implements ClientAuthorisationProvider {
	private final Logger logger = LoggerFactory.getLogger(PredefinedClientAuthorisationProvider.class);

	private final String header;
	private final String value;

	public PredefinedClientAuthorisationProvider(String header, String value) {
		this.header = header;
		this.value = value;
	}

	@Override
	public void provideAuthorisation(ClientRequestContext requestContext) {
		MultivaluedMap<String, Object> headers = requestContext.getHeaders();
		if (!headers.containsKey(header)) {
			headers.putSingle(header, value);
		}
	}
}
