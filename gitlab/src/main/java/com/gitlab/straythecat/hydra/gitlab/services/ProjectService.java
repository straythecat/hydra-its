package com.gitlab.straythecat.hydra.gitlab.services;

import org.springframework.stereotype.Service;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.WebTarget;

@Service
public class ProjectService {
	private final WebTarget projects;
	private final WebTarget project;

	public ProjectService(WebTarget service) {
		projects = service.path("/projects").
				queryParam("with_issues_enabled", true).
				queryParam("visibility", "public").
				queryParam("membership", true);
		project = service.path("/projects/{id}");
	}

	public JsonArray getProjects() {
		return projects.request().get(JsonArray.class);
	}

	public JsonObject getProject(long id) {
		return project.resolveTemplate("id", id).request().get(JsonObject.class);
	}
}
