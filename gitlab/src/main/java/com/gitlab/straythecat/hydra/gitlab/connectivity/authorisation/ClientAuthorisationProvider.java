package com.gitlab.straythecat.hydra.gitlab.connectivity.authorisation;

import javax.ws.rs.client.ClientRequestContext;

public interface ClientAuthorisationProvider {
	void provideAuthorisation(ClientRequestContext request);
}
