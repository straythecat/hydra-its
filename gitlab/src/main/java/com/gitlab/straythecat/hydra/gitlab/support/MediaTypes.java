package com.gitlab.straythecat.hydra.gitlab.support;

import org.springframework.http.MediaType;

public class MediaTypes {
	public final static String JSON_LD_VALUE = "application/ld+json";
	public final static MediaType JSON_LD = MediaType.valueOf(JSON_LD_VALUE);
}
