package com.gitlab.straythecat.hydra.gitlab.services;

import com.gitlab.straythecat.hydra.gitlab.connectivity.support.collections.ArrayMap;
import org.springframework.stereotype.Service;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.WebTarget;
import java.util.Map;

@Service
public class UserService {
	private final WebTarget members;
	private final WebTarget member;

	public UserService(WebTarget service) {
		members = service.path("/projects/{projectId}/members");
		member = service.path("/projects/{projectId}/members/{userId}");
	}

	public JsonArray getUsers(long projectId) {
		return members.resolveTemplate("projectId", projectId).request().get(JsonArray.class);
	}

	public JsonObject getUser(long projectId, long userId) {
		Map<String, Object> parameters = ArrayMap.create(
				ArrayMap.entry("projectId", projectId),
				ArrayMap.entry("userId", userId)
		);
		return member.resolveTemplates(parameters).request().get(JsonObject.class);
	}
}
