package com.gitlab.straythecat.hydra.gitlab.connectivity.support;

import com.gitlab.straythecat.hydra.gitlab.connectivity.authorisation.ClientAuthorisationProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

public class ContextClientAuthorisationProvider implements ClientAuthorisationProvider {
	private final Logger logger = LoggerFactory.getLogger(ContextClientAuthorisationProvider.class);

	private final String header;
	private final HttpServletRequest requestProxy;

	public ContextClientAuthorisationProvider(HttpServletRequest requestProxy) {
		this(requestProxy, HttpHeaders.AUTHORIZATION);
	}

	public ContextClientAuthorisationProvider(HttpServletRequest requestProxy, String header) {
		this.header = header;
		this.requestProxy = requestProxy;
	}

	@Override
	public void provideAuthorisation(ClientRequestContext requestContext) {
		String authorisation = null;

		try {
			authorisation = requestProxy.getHeader(header);
		} catch (IllegalStateException exception) {
			logger.error("Thread-bound request has been not found.", exception);
		}

		if (authorisation != null) {
			MultivaluedMap<String, Object> headers = requestContext.getHeaders();
			if (!headers.containsKey(header)) {
				headers.putSingle(header, authorisation);
			}
		}
	}
}
