package com.gitlab.straythecat.hydra.gitlab.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Component
public class LoggingClientResponseFilter implements ClientResponseFilter {
	private final Logger logger = LoggerFactory.getLogger(LoggingClientResponseFilter.class);

	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		Response.StatusType status = responseContext.getStatusInfo();
		if (status.getFamily() == Response.Status.Family.CLIENT_ERROR || status.getFamily() == Response.Status.Family.SERVER_ERROR) {
			logger.warn("Request: {} {} | Response: {} {}", requestContext.getMethod(), requestContext.getUri().toString(),
					status.getStatusCode(), status.getReasonPhrase());
		}
	}
}
