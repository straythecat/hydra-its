package com.gitlab.straythecat.hydra.gitlab.resources;

import com.gitlab.straythecat.hydra.gitlab.services.IssueService;
import com.gitlab.straythecat.hydra.gitlab.support.MediaTypes;
import com.gitlab.straythecat.json.transformation.JsonTransformation;
import com.gitlab.straythecat.json.transformation.JsonTransformations;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.json.*;

import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.fromMethodCall;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@RestController
@CrossOrigin(exposedHeaders = HttpHeaders.LINK)
@RequestMapping(produces = MediaTypes.JSON_LD_VALUE)
public class IssueResource {
	private final IssueService service;
	private final JsonTransformations transformations;
	private final JsonBuilderFactory json;

	private final JsonTransformation issueTransformation;
	private final JsonTransformation issueCreationTransformation;

	public IssueResource(IssueService service, JsonTransformations transformations, JsonBuilderFactory json) {
		this.service = service;
		this.transformations = transformations;
		this.json = json;

		issueTransformation = transformations.compound(
				transformations.create("@context", () ->
						Json.createValue(fromMethodCall(on(ContextResource.class).getIssueContext()).toUriString())),
				transformations.create("@type", Json.createValue("Issue")),
				(o, b) -> b.add("@id", Json.createValue(
						fromMethodCall(on(IssueResource.class).
								getIssue(o.getJsonNumber("project_id").longValueExact(),
										o.getJsonNumber("iid").longValueExact())).toUriString())),
				transformations.retain("title"),
				transformations.retain("description"),
				transformations.retain("state")
		);

		issueCreationTransformation = transformations.compound(
				transformations.requireNewName("http://schema.org/name", "title"),
				transformations.rename("http://schema.org/description", "description")
		);
	}

	@GetMapping(path = "/projects/{projectId}/issues")
	public JsonObject getIssues(@PathVariable("projectId") long projectId) {
		JsonArray issues = service.getIssues(projectId);
		return json.createObjectBuilder().
				add("@context", fromMethodCall(on(ContextResource.class).getIssueCollectionContext()).toUriString()).
				add("@type", "IssueCollection").
				add("@id", fromMethodCall(on(IssueResource.class).getIssues(projectId)).toUriString()).
				add("issues", transformations.apply(issueTransformation, issues)).
				build();
	}

	@GetMapping("/projects/{projectId}/issues/{issueId}")
	public JsonObject getIssue(@PathVariable("projectId") long projectId, @PathVariable("issueId") long issueId) {
		JsonObject issue = service.getIssue(projectId, issueId);
		issue = transformations.apply(issueTransformation, issue);
		return issue;
	}

	@PostMapping(path = "/projects/{projectId}/issues", consumes = MediaTypes.JSON_LD_VALUE)
	public JsonObject createIssue(@PathVariable("projectId") long projectId, @RequestBody JsonObject issue) {
		issue = transformations.apply(issueCreationTransformation, issue);
		issue = service.createIssue(projectId, issue);
		return transformations.apply(issueTransformation, issue);
	}
}
