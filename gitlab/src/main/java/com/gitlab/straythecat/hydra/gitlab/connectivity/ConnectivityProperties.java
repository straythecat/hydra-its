package com.gitlab.straythecat.hydra.gitlab.connectivity;

import java.net.URI;

public interface ConnectivityProperties {
	URI getServiceUrl();
	String getAuthorisationHeader();
	String getAuthorisationValue();
}
