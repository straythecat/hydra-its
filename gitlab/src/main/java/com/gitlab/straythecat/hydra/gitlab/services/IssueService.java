package com.gitlab.straythecat.hydra.gitlab.services;

import com.gitlab.straythecat.hydra.gitlab.connectivity.support.collections.ArrayMap;
import org.springframework.stereotype.Service;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import java.util.Map;

@Service
public class IssueService {
	private final WebTarget issues;
	private final WebTarget issue;

	public IssueService(WebTarget service) {
		issues = service.path("/projects/{projectId}/issues");
		issue = service.path("/projects/{projectId}/issues/{issueId}");
	}

	public JsonArray getIssues(long projectId) {
		return issues.resolveTemplate("projectId", projectId).request().get(JsonArray.class);
	}

	public JsonObject getIssue(long projectId, long issueId) {
		Map<String, Object> parameters = ArrayMap.create(
				ArrayMap.entry("projectId", projectId),
				ArrayMap.entry("issueId", issueId)
		);
		return issue.resolveTemplates(parameters).request().get(JsonObject.class);
	}

	public JsonObject createIssue(long projectId, JsonObject issue) {
		return issues.resolveTemplate("projectId", projectId).request().post(Entity.json(issue), JsonObject.class);
	}
}
