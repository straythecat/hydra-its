package com.gitlab.straythecat.hydra.gitlab.connectivity;

import com.gitlab.straythecat.hydra.gitlab.connectivity.authorisation.ClientAuthorisationProvider;
import com.gitlab.straythecat.hydra.gitlab.connectivity.authorisation.ClientAuthorisationRequestFilter;
import com.gitlab.straythecat.hydra.gitlab.connectivity.support.ContextClientAuthorisationProvider;
import com.gitlab.straythecat.hydra.gitlab.connectivity.support.PredefinedClientAuthorisationProvider;
import com.gitlab.straythecat.hydra.gitlab.logging.LoggingClientResponseFilter;
import com.gitlab.straythecat.hydra.gitlab.serialisation.ObjectMapperProvider;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@Configuration
public class ConnectivityConfiguration {
	private final ClientBuilder clientBuilder;

	public ConnectivityConfiguration(ObjectMapperProvider mapperProvider, LoggingClientResponseFilter loggingFilter, HttpClientProperties httpClientProperties) {
		clientBuilder = ClientBuilder.newBuilder().
				register(mapperProvider).
				register(loggingFilter);

		((ResteasyClientBuilder) clientBuilder).
				connectionPoolSize(httpClientProperties.getConnectionPoolSize()).
				maxPooledPerRoute(httpClientProperties.getMaxPooledPerRoute());
	}

	@Bean(destroyMethod = "close")
	public Client client(){
		return clientBuilder.build();
	}

	@Bean
	public WebTarget service(Client client, ConnectivityProperties properties, HttpServletRequest requestProxy) {
		WebTarget service = client.target(properties.getServiceUrl());

		ClientAuthorisationProvider provider;
		if (properties.getAuthorisationHeader() == null) {
			provider = new ContextClientAuthorisationProvider(requestProxy);
		} else if (properties.getAuthorisationValue() == null) {
			provider = new ContextClientAuthorisationProvider(requestProxy, properties.getAuthorisationHeader());
		} else {
			provider = new PredefinedClientAuthorisationProvider(properties.getAuthorisationHeader(), properties.getAuthorisationValue());
		}
		return service.register(new ClientAuthorisationRequestFilter(provider));
	}
}
