package com.gitlab.straythecat.hydra.gitlab.connectivity.authorisation;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;

public class ClientAuthorisationRequestFilter implements ClientRequestFilter {
	private final ClientAuthorisationProvider provider;

	public ClientAuthorisationRequestFilter(ClientAuthorisationProvider provider) {
		this.provider = provider;
	}

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		if (provider != null) {
			provider.provideAuthorisation(requestContext);
		}
	}
}
