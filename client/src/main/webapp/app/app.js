(function () {
	'use strict';

	angular.module('hydraApp', [
		'ngAnimate',
		'ui.bootstrap',
		'main.module'
	]);

})();
