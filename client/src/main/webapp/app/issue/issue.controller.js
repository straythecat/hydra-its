(function () {
	'use strict';

	angular
		.module('main.module')
		.controller('IssueDialogController', ['$uibModalInstance', IssueDialogController]);

	function IssueDialogController($modalInstance) {
		var vm = this;

		vm.submit = submit;
		vm.dismiss = dismiss;

		initialise();

		function initialise() {
			vm.issue = {};
		}

		function submit() {
			$modalInstance.close(vm.issue);
		}

		function dismiss() {
			$modalInstance.dismiss();
		}
	}

})();
