(function () {
	'use strict';

	angular
		.module('main.module')
		.controller('MainController', ["$q", "$uibModal", MainController]);

	function MainController($q, $modal) {
		const vm = this;

		vm.loadEntry = loadEntry;
		vm.select = select;
		vm.viewProjectIssues = viewProjectIssues;
		vm.createIssue = createIssue;
		vm.viewProjectMembers = viewProjectMembers;

		initialise();

		function reset() {
			vm.entry = null;
			vm.context = null;
			vm.issueCollection = null;

			vm.menu	= [];
			vm.view = null;
			vm.loading = false;

			vm.projects = null;
			vm.issues = null;
			vm.users = null;
		}

		function initialise() {
			vm.entryUrl = null;

			reset();
		}

		function loading() {
			vm.loading = true;
		}

		function stopped() {
			vm.loading = false;
		}

		function loadEntry() {
			reset();
			loading();
			$q.when(hydra.loadDocument(vm.entryUrl), function (document) {
				vm.context = {'@context': {'@vocab': document.api.iri}};
				$q.when(hydra.model.create(document.classes, vm.context), function (entry) {
					vm.entry = entry;
					addMenuItem(entry['EntryPoint/projects'], 'Projects', 'projects', 'app/projects.tmpl.html');
					addMenuItem(entry['EntryPoint/issues'], 'Issues', 'issues', 'app/issues.tmpl.html');
					addMenuItem(entry['EntryPoint/users'], 'Users', 'users', 'app/users.tmpl.html');

					vm.projectSupportsMembers = supports(vm.entry.api.iri + 'Project', vm.entry.api.iri + 'Project/members');
					vm.issueSupportsState = supports(vm.entry.api.iri + 'Issue', vm.entry.api.iri + 'Issue/state');

					stopped();
				}, function (error) {
					stopped();
					console.error(error);
				})
			}, function (error) {
				stopped();
				console.error(error);
			});
		}

		function supports(entity, property) {
			return vm.entry.api.findClass(entity).findProperty(property) !== undefined;
		}

		function addMenuItem(resource, label, propertyName, template) {
			if (resource !== undefined) {
				vm.menu.push({
					label: label,
					view: {
						label: label,
						template: template,
						load: loadCollection(resource, propertyName)
					}
				});
			}
		}

		function loadCollection(resource, members, collection) {
			return function () {
				loading();
				$q.when(resource['@get'](vm.context)).then(function (entities) {
					const result = entities['http://www.w3.org/ns/hydra/core#member'];
					vm[members] = Array.isArray(result) ? result : [result];

					if (collection !== undefined && collection !== null) {
						vm[collection] = entities;
					}
					stopped();
				}, function (error) {
					stopped();
					console.error(error);
				});
			}
		}

		function select(property, item) {
			if (vm[property] !== null) {
				vm[property].selected = false;
			}
			vm[property] = item;
			item.selected = true;
		}

		function viewProjectIssues(project) {
			loading();
			const link = project['Project/issues']['@id'];
			$q.when(hydra.model.load(link, vm.context), function (issues) {
				const load = loadCollection(issues, 'issues', 'issueCollection');

				select('view', {
					label: 'Issues of project ' + project['http://schema.org/name'],
					template: 'app/issues.tmpl.html',
					load: load
				});

				load();
			}, function (error) {
				stopped();
				console.error(error);
			});
		}

		function createIssue() {
			$modal.open({
				templateUrl: 'app/issue/issue.tmpl.html',
				controller: 'IssueDialogController',
				controllerAs: 'vm',
				animation: true,
				size: 'lg'
			}).result.then(function (issue) {
				loading();
				issue['@context'] = {
					name: 'http://schema.org/name',
					description: 'http://schema.org/description'
				};
				$q.when(hydra.model.create(vm.entry.api.findClass(vm.entry.api.iri + 'Issue'), issue), function (issue) {
					$q.when(vm.issueCollection['@post'](issue), function () {
						vm.view.load();
					}, function (error) {
						stopped();
						console.error(error);
					});
				}, function (error) {
					stopped();
					console.error(error);
				});
			});
		}

		function viewProjectMembers(project) {
			loading();
			const link = project['Project/members']['@id'];
			$q.when(hydra.model.load(link, vm.context), function (members) {
				const load = loadCollection(members, 'users');

				select('view', {
					label: 'Members of project ' + project['http://schema.org/name'],
					template: 'app/users.tmpl.html',
					load: load
				});

				load();
			}, function (error) {
				stopped();
				console.error(error);
			});
		}

	}

})();
